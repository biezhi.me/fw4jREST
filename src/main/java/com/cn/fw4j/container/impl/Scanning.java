package com.cn.fw4j.container.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.cn.fw4j.container.inter.IContainer;
import com.cn.fw4j.container.inter.IContainerObserver;
import com.cn.fw4j.util.ScanningFileHelper;
import com.cn.fw4j.util.oro.Perl5;

/**
 * 扫描文件
 * @author 穆仁超
 *
 */
public class Scanning implements IContainer{
	//private final static Logger logger = LoggerFactory.getLogger(ScanningFile.class);
	
	private List<IContainerObserver> objs = new LinkedList<IContainerObserver>();
	
	@Override
	public void register(IContainerObserver obj) {
		objs.add(obj);
	}
	
	public void run(){
		Set<String> fs = ScanningFileHelper.getFileSet();	
		StringBuilder r = new StringBuilder("/");
		r.append("^(\\w|/)+:?/(\\w|\\.|/|-)+/(\\w|/)*");
		r.append("/(\\w|/)+");
		r.append("(.class|.groovy)$");
		r.append("/");
		String regex = r.toString();
		Iterator<String> it = fs.iterator();
		while(it.hasNext()){
			String s = (String)it.next();
			boolean b = Perl5.match(s,regex);
			if(b){
				for(IContainerObserver ctrl : objs){
					ctrl.add(s);
				}
			}
		}
	}
}
