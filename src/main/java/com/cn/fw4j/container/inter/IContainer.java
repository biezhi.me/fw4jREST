package com.cn.fw4j.container.inter;


/**
 * Controller注册
 * @author murenchao
 */
public interface IContainer {
	void register(IContainerObserver obj);
	void run();
}