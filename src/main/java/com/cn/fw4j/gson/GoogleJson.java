package com.cn.fw4j.gson;

import com.cn.fw4j.json.inter.IFw4jJson;
import com.google.gson.Gson;

public class GoogleJson implements IFw4jJson {

	@Override
	public String toJson(Object obj) {
		Gson g = new Gson();
		return g.toJson(obj);
	}
	

}
