package com.cn.fw4j.ioc.impl;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cn.fw4j.ioc.inter.IFw4jSpringPlugs;

public class Fw4jSpringPlugs implements IFw4jSpringPlugs {
	private  final static ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");;
	
	@Override
	public Object getBean(String name, Class<?> arg1) {
		try{
		return context.getBean(name,arg1);  
		}catch(BeansException ex){
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public Object getBean(String name) {
		Object obj = null;
		try{
			obj = context.getBean(name);  
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return obj;
	}

}
