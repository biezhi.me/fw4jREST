package com.cn.fw4j.mvc.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.cn.fw4j.mvc.servlet.impl.DispatcherHandle;
import com.cn.fw4j.mvc.servlet.inter.IDispatcherHandle;

/**
 * @author 穆仁超
 * Servlet调度器
 * Email : minix3@126.com
 * QQ : 505955077
 */
public final class DispatcherServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static IDispatcherHandle dispatcher = new DispatcherHandle(); 
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		dispatcher.delete(request, response);
	}

	@Override
	protected void doHead(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
	
	@Override
	protected final void doPut(final HttpServletRequest request,final HttpServletResponse response) 
			throws ServletException,IOException{
		dispatcher.put(request, response);
	}
	@Override
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException{
		dispatcher.get(request, response);
	}

	@Override
	protected final void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException{
		dispatcher.post(request, response);
	}
}