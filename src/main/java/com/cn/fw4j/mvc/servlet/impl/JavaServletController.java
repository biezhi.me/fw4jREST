package com.cn.fw4j.mvc.servlet.impl;


import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cn.fw4j.container.impl.JavaController;
import com.cn.fw4j.container.inter.IController;
import com.cn.fw4j.ioc.annotation.Fw4jSpringIoc;
import com.cn.fw4j.ioc.impl.Fw4jSpringPlugs;
import com.cn.fw4j.ioc.inter.IFw4jSpringPlugs;
import com.cn.fw4j.mvc.page.inter.IPageTemplate;
import com.cn.fw4j.mvc.servlet.inter.IServletController;
import com.cn.fw4j.util.ClassHelper;
import com.cn.fw4j.util.request.parameter.OutParam;

public class JavaServletController extends ServletController implements IServletController{
	
	private final static Logger logger = LoggerFactory.getLogger(JavaServletController.class);
	private final static IController ic = JavaController.getInstance(); 

	@Override
	public void controller(HttpServletRequest request,
			HttpServletResponse response,String urlPath) {
		try {
			String call = getValue(urlPath);
	    	if(call == null) return;
	    	OutParam out = newOutParam(request);
			out.put("basePath", basePath(request));
 			String[] c  = call.split("[$]");
 			Class<?> cl = ClassHelper.loadClass(c[0]);
 			logger.debug("create class"+cl.getSimpleName());
 			String template = getTemplate(cl);
 			Object instance = cl.newInstance();
 			Method m = ClassHelper.getMethodByName(cl.getMethods(), c[1]);
 			if(m != null){
 				List<Object> args = parameters(request, response, m,out);
				this.decorator(m, args);
				this.setProperty(cl.getDeclaredFields(), instance);
				Object obj = null;
				if(args != null)
					obj = m.invoke(instance,args.toArray());
				else
					obj = m.invoke(instance);
				if(c.length>2)
					printJson(response, c[2], obj);
				if(obj != null && (obj.toString().startsWith("request:")||obj.toString().startsWith("redirect:"))){
					String path  = obj.toString();
					Class<?> pcl = ClassHelper.loadClass(template);
					IPageTemplate ipt = (IPageTemplate) pcl.newInstance();
					ipt.Template(path, out, request, response);
				}
 			}
 		} catch (Exception e){
 			e.printStackTrace();
 		}
	}
	
	/**
	 * 根据注解设置字段的值
	 * @param fields
	 * @param instance
	 */
	private void setProperty(Field[] fields,Object instance){
		for(Field field : fields){
			Fw4jSpringIoc ioc =	field.getAnnotation(Fw4jSpringIoc.class);
			if(ioc != null){
				field.setAccessible(true);
				IFw4jSpringPlugs ifsp = new Fw4jSpringPlugs();
				Object obj = ifsp.getBean(ioc.value());
				try {
					field.set(instance, obj);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 获取url对应的类
	 * @param urlPath
	 * @return
	 */
	private String getValue(String urlPath){
		   if(ic != null){
				if(urlPath.startsWith("GET")){
					return ic.gets(urlPath);
				}else if(urlPath.startsWith("POST")){
					return ic.posts(urlPath);
				}else if(urlPath.startsWith("PUT")){
					return ic.puts(urlPath);
				}else if(urlPath.startsWith("DELETE")){
					return ic.deletes(urlPath);
				}
		   }
		   return null;
	}
}