package com.cn.fw4j.util;

import java.io.IOException;
import java.util.Properties;

public class PropertiesHelper {
	public static String get(String filename,String key){
		if(filename != null){
			try {
				Properties p = new Properties();
				p.load(StreamHelper.getInputStreamByFileName(filename));
				if(p.containsKey(key)){
					return p.getProperty(key);
				}
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
}
