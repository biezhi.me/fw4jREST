package com.cn.fw4j.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StreamHelper {
	public static String getContent(InputStream input) throws IOException{
		StringBuilder sb = new StringBuilder();
		InputStreamReader isr = new InputStreamReader(input);
		BufferedReader br = new BufferedReader(isr);
		String line = null;
		while((line = br.readLine()) != null){
			sb.append(line);
		}
		isr.close();
		br.close();
		input.close();
		return sb.toString();
	}
	
	public static InputStream getInputStreamByFileName(String name){
		return Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
	}
	
	public static String getFilePathByFileName(String name){
		return Thread.currentThread().getContextClassLoader().getResource(name).getPath();
	}
}
